import os
import commands
import shutil
from backend import Backend

class OpenVPN:

    def __init__(self, name):
        self.name = name
        self.path = "/etc/openvpn/server_%s/" % self.name
        self.clients_path = "/etc/openvpn/server_%s/clients/" % self.name
        self.config_file = "%s/openvpn.cfg" % self.path
        self.systemctl_service = "ovpn_server_%s.service" % name
        self.systemctl_file = "/etc/systemd/system/%s.service" % self.systemctl_service
        self.read_config()
    
    def create(self):
        if not os.path.isdir(self.path):
            os.makedirs(self.path)
        self.create_openvpn_config()
        self.create_systemd_service()

    def create_openvpn_config(self):
        dir_path = os.path.dirname(os.path.realpath(__file__))
        ovpn_sample_file =  "%s/openvpn_server_sample_file.conf" % dir_path
        shutil.copy(ovpn_sample_file, self.config_file)
    
    def create_systemd_service(self):
        dir_path = os.path.dirname(os.path.realpath(__file__))
        systemd_sample_file =  "%s/systemctl_sample_file.service" % dir_path
        shutil.copy(systemd_sample_file, self.systemctl_file)

        self.replace_in_file(self.systemctl_file, "<desc>", "OpenVPN Server (name: %s) created by ovpnui." % self.name)
        self.replace_in_file(self.systemctl_file, "<working-dir>", self.path)
        self.replace_in_file(self.systemctl_file, "<status-log>", "%s/status.log" % self.path)
        self.replace_in_file(self.systemctl_file, "<config-path>", self.config_file)
        commands.getstatusoutput("systemctl daemon-reload")
        commands.getstatusoutput("systemctl enable %s" % self.systemctl_service)
        commands.getstatusoutput("systemctl start %s" % self.systemctl_service)
    
    def read_config(self):
        self.port = self.search_config_var("port")

    def search_config_var(self, name):
        if os.path.isfile(self.config_file):
            cmd = commands.getstatusoutput("cat %s | grep ^%s" % (self.config_file, name))
            return cmd[1].split()[1]
        else:
            return ""
    
    def replace_in_file(self, f, search, replace):
        # Read in the file
        with open(f, 'r') as file :
            filedata = file.read()

        # Replace the target string
        filedata = filedata.replace(search, replace)

        # Write the file out again
        with open(f, 'w') as file:
            file.write(filedata)
    
    def get_status(self):
        cmd = commands.getstatusoutput("systemctl status %s | egrep -oe 'Active: [a-z]+'" % self.systemctl_service)
        return cmd[1].split()[1] == "active"
    
    def delete(self):
        pass
    
    def get_clients(self):
        b = Backend()
        clients = b.get_servers_clients(self.name)
        return clients