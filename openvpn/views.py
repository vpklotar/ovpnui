# -*- coding: utf-8 -*-
from django.shortcuts import render, redirect
from django.views.generic import TemplateView
from django.views.generic.base import View
from django.views.generic.edit import CreateView
from django.http import HttpResponse, Http404

import os
import commands

from forms import create_new_server_form, create_new_client_form

from openvpn.backend import Backend
from openvpn.ovpn import OpenVPN

class Index(TemplateView):
    def get_data(self):
        b = Backend()
        errors = list()
        if not b.openvpn_installed():
            errors.append({'title': 'OpenVPN is not installed.', 'action': 'Run the command "yum install openvpn".'})
        if not b.openssl_installed():
            errors.append({'title': 'OpenSSL is not installed.', 'action': 'Run the command "yum install openssl".'})
        
        return {'errors': errors}

    def get(self, request, **kwargs):
        data = self.get_data()
        return render(request, "index.html", {'title': "OpenVPN Configuration", 'data': data, 'num_errors': len(data['errors'])})

class Servers(TemplateView):
    def get(self, request, **kwargs):
        b = Backend()
        all_keys = b.get_all_servers()
        vpns = list()
        for name in all_keys:
            ovpn_name = name[7:]
            vpn = OpenVPN(name=ovpn_name)
            vpns.append({
                "name": ovpn_name,
                "running": vpn.get_status(),
                "port": vpn.port
            })
        return render(request, "servers.html", {'title': "OpenVPN Servers", 'vpns': vpns})

class create_server(CreateView):
    def get(self, request, **kwargs):
        form = create_new_server_form()
        return render(request, "create_server.html", {'title': "Create new server", 'form': form})
    
    def post(self, request, **kwargs):
        b = Backend()
        b.create_new_server(args=request.POST)
        return redirect("Servers")

class download_client(TemplateView):
    def get(self, request, **kwargs):
        server_name = kwargs['name']
        client_name = request.GET['client_name']
        print("Downloading client '%s' for server '%s'." % (client_name, server_name))
        ovpn = OpenVPN(server_name)
        file_path = "%s/%s.p12" % (ovpn.clients_path, client_name)
        if os.path.exists(file_path):
            b = Backend()
            data = b.create_client_inline_configuration(server_name, client_name)
            response = HttpResponse(data)
            response['Content-Disposition'] = 'inline; filename=%s.ovpn' % client_name
            response['Content-Type'] = "application/openvpn"
            return response
        raise Http404

class CAs(TemplateView):
    def get(self, request, **kwargs):
        return render(request, "cas.html", {'title': "OpenVPN CAs"})

class server_view(TemplateView):
    def get(self, request, **kwargs):
        server = kwargs.get('name', None)
        ovpn = OpenVPN(server)

        return render(request, "server_view.html", {
            'title': "OpenVPN Server '%s'" % server,
            'name': server,
            'running': ovpn.get_status(),
            'systemctl_name': ovpn.systemctl_service,
            'clients': ovpn.get_clients()
            })

class create_client(CreateView):
    def get(self, request, **kwargs):
        form = create_new_client_form()
        form.fields['name'].initial = kwargs['name'].decode('UTF-8')
        return render(request, "create_client.html", {'form': form})
    
    def post(self, request, **kwargs):
        init_args = {
            'server_name': kwargs['name'].decode('UTF-8'),
            'client_name': request.POST['client_name'],
            'key_size': request.POST['key_size'],
            'dh_size': request.POST['dh_size'],
        }
        b = Backend()
        b.create_client_certificate(args=init_args)
        return redirect("server_view", name=init_args['server_name'])

class CreateKeys(TemplateView):
    def get(self, request, **kwargs):
        name = kwargs.get('name', 'server')
        print("Creating new keys with name: %s" % name)
        cn = "10.0.0.14"
        b = Backend()
        b.create_new_server(name)
        keys_moved = True
        return render(request, "cas_create.html", {'title': "OpenVPN CA Creation", 'result': keys_moved})