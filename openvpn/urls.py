from django.conf.urls import url
from django.contrib import admin
from openvpn import views

urlpatterns = [
    url(r'^$', views.Index.as_view(), name="Index"),
    url(r'^Servers/create$', views.create_server.as_view(), name="create_server"),
    url(r'^Servers/(?P<name>\w{0,50})$', views.server_view.as_view(), name="server_view"),
    url(r'^Servers/(?P<name>\w{0,50})/download/$', views.download_client.as_view(), name="create_new_client"),
    url(r'^Servers/(?P<name>\w{0,50})/create_client$', views.create_client.as_view(), name="create_new_client"),
    url(r'^Servers$', views.Servers.as_view(), name="Servers"),
    url(r'^CAs$', views.CAs.as_view(), name="CAs"),
    url(r'^create_keys/(?P<name>\w{0,50})$', views.CreateKeys.as_view(), name="create_keys"),
]
