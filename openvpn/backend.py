from subprocess import check_output

from choices import COUNTRY_CHOICES

import commands
import os
import subprocess
import shutil

class Backend:
    def openvpn_installed(self):
        return_code,output = commands.getstatusoutput("rpm -qa | grep openvpn")
        if output:
            return True
        else:
            return False
    
    def openssl_installed(self):
        return_code,output = commands.getstatusoutput("rpm -qa | grep openssl")
        if output:
            return True
        else:
            return False
        return False
    
    def get_client_args_base(self, server_name):
        return_code,output = commands.getstatusoutput("openssl x509 -in /etc/openvpn/server_%s/server/server.cer -text -noout | grep 'Subject:' | sed 's/Subject://g' | sed 's/\/emailAddress/, emailAddress/g'" % server_name)
        if output:
            parts = {}
            for part in output.split(','):
                spl = part.strip().split('=')
                parts[spl[0]] = spl[1]
            return parts

    def get_ca_environment(self, args):
        d = dict(os.environ)
        if type(args['country']) == str:
            country = args['country']
        else:
            country = COUNTRY_CHOICES[int(args["country"])-1][1]
        # Below is CA variables
        d["KEY_CN"] = args["name"].decode("UTF-8")
        d["KEY_COUNTRY"] = country
        d["KEY_PROVINCE"] = args["province"].decode("UTF-8")
        d["KEY_CITY"] = args["city"].decode("UTF-8")
        d["KEY_ORG"] = args["orginization"].decode("UTF-8")
        d["KEY_OU"] = args["orginizational_unit"].decode("UTF-8")
        d["KEY_EMAIL"] = args["email"].decode("UTF-8")

        d["CA_ROOT_DIRECTORY"] = "/etc/openvpn/server_%s" % args["name"].decode("UTF-8")
        d["OPENVPN_BIN"] = "/usr/sbin/openvpn"
        d["OPENSSL_BIN"] = "/usr/bin/openssl"
        d["CA_NAME"] = "ca"
        d["CA_COMMON_DIR"] = "%s/common" % d["CA_ROOT_DIRECTORY"]
        d["CA_CLIENTS_DIR"] = "%s/clients" % d["CA_ROOT_DIRECTORY"]
        d["CA_SERVER_DIR"] = "%s/server" % d["CA_ROOT_DIRECTORY"]
        d["KEY_BITSIZE"] = args["key_size"].decode("UTF-8")
        d["DH_KEYSIZE"] = args["dh_size"].decode("UTF-8")
        d["CERTIFICATE_LIFETIME"] = "3650"
        d["OPENVPN_CONF"] = "%s/openvpn.cfg" % d["CA_ROOT_DIRECTORY"]
        d["OPENSSL_CONF"] = "%s/openssl.cfg" % d["CA_ROOT_DIRECTORY"]
        d["OPENSSL_CONF_X509_V3_EXT_SERVER"] = "%s/x509v3_server.ext" % d["CA_COMMON_DIR"]
        d["OPENSSL_CONF_X509_V3_EXT_CLEIENT"] = "%s/x509v3_client.ext" % d["CA_COMMON_DIR"]
        d["CA_CRL"] = "%s/crl.pem" % d["CA_COMMON_DIR"]
        d["CA_PASSWORD"] = "password"

        return d
    
    def create_new_server(self, args):
        d = self.get_ca_environment(args=args)
        dir_path = os.path.dirname(os.path.realpath(__file__))
        proc = subprocess.Popen(["./create_base_ca.sh"], env=d, cwd="%s/helpers/" % dir_path)
        proc.wait()
        proc = subprocess.Popen(["./create_openvpn_server.sh"], env=d, cwd="%s/helpers/" % dir_path)
        proc.wait()

    def create_client_certificate(self, args):
        server_name = args['server_name']
        client_args_base = self.get_client_args_base(server_name)
        init_args = {
            'name': server_name,
            'country': client_args_base['C'],
            'province': client_args_base['ST'],
            'city': client_args_base['L'],
            'orginization': client_args_base['O'],
            'orginizational_unit': client_args_base['OU'],
            'email': client_args_base['emailAddress'],
            'key_size': args['key_size'],
            'dh_size': args['dh_size']
        }
        d = self.get_ca_environment(args=init_args)
        d['CLIENT_NAME'] = args['client_name'].decode('UTF-8')
        dir_path = os.path.dirname(os.path.realpath(__file__))
        proc = subprocess.Popen(["./create_client_certificate.sh"], env=d, cwd="%s/helpers/" % dir_path)
        proc.wait()
    
    def get_servers_clients(self, server_name):
        server_path = "/etc/openvpn/server_%s" % server_name
        clients_path = "%s/clients/" % server_path
        clients = list()
        for f in os.listdir(clients_path):
            if f.endswith(".p12"):
                clients.append({
                    'name': f[:-4]
                })
        return clients
    
    def get_all_servers(self):
        sub_dirs = [x[0] for x in os.walk("/etc/openvpn/")]
        re = list()
        for sub_dir in sub_dirs:
            dir_name = str(sub_dir.split('/')[-1])
            if dir_name.startswith("server_"):
                re.append(dir_name)
        return re
    
    def create_client_inline_configuration(self, server_name, client_name):
        dir_path = os.path.dirname(os.path.realpath(__file__))
        return_code,output = commands.getstatusoutput("%s/helpers/create_client_inline.sh %s %s" % (dir_path, server_name, client_name))
        return output